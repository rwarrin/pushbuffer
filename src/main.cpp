#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define Assert(expression) if(!(expression)) { *(int *)0 = 0; }

#define Kilobyte(Value) (Value * 1024LL)
#define Megabyte(Value) (Kilobyte(Value) * 1024LL)

#define true 1
#define false 0

#define InvalidCodePath Assert(!"InvalidCodePath")
#define InvalidDefaultCase default: {Assert(!"InvalidDefaultCase");} break

typedef uint8_t uint8;

enum push_buffer_entry_type
{
	PushBufferEntryType_buffer_entry_first,
	PushBufferEntryType_buffer_entry_second,
	PushBufferEntryType_buffer_entry_third,
};

struct push_buffer_header
{
	push_buffer_entry_type Type;
};

struct buffer_entry_first
{
	push_buffer_header Header;
	int X;
	int Y;
};

struct buffer_entry_second
{
	push_buffer_header Header;
	char Name[64];
	int Count;
};

struct buffer_entry_third
{
	push_buffer_header Header;
	uint8 Level;
	int Experience;
	int Health;
	int Mana;
	char Race[16];
	char Class[16];
};

struct push_group
{
	int MaxPushBufferSize;
	int PushBufferSize;
	uint8 *PushBufferBase;
};

struct application_memory
{
	int PermanentStorageSize;
	void *PermanentStorage;
	int TransientStorageSize;
	void *TransientStorage;
};

struct memory_arena
{
	size_t Size;
	uint8 *Base;
	size_t Used;

	int TempCount;
};

typedef uint32_t bool32;
struct transient_state
{
	bool32 IsInitialized;
	memory_arena TranArena;
};

struct temporary_memory
{
	memory_arena *Arena;
	size_t Used;
};

#define PushStruct(Arena, type) (type *)Push_(Arena, sizeof(type))
#define PushArray(Arena, Count, type) (type *)Push_(Arena, (Count)*sizeof(type))
#define PushSize(Arena, Size) Push_(Arena, Size)
inline void *
Push_(memory_arena *Arena, size_t Size)
{
	Assert((Arena->Used + Size) <= Arena->Size);
	void *Result = Arena->Base + Arena->Used;
	Arena->Used += Size;

	return(Result);
}

inline void
ZeroMemory(void *Memory, size_t Count)
{
	uint8 *MemPtr = (uint8 *)Memory;

	// TODO(rick): This could be improved by unrolling the loop
	for(int MemoryIndex = 0;
		MemoryIndex < Count;
		++MemoryIndex)
	{
		*MemPtr++ = 0x00;
	}
}

void
InitializeArena(memory_arena *Arena, size_t Size, void *Base)
{
	Arena->Size = Size;
	Arena->Base = (uint8 *)Base;
	Arena->Used = 0;
	Arena->TempCount = 0;
}

temporary_memory
BeginTemporaryMemory(memory_arena *Arena)
{
	temporary_memory Result;

	Result.Arena = Arena;
	Result.Used = Arena->Used;

	++Arena->TempCount;

	return(Result);
}

void
EndTemporaryMemory(temporary_memory TempMem)
{
	memory_arena *Arena = TempMem.Arena;
	Assert(Arena->Used >= TempMem.Used);
	Arena->Used = TempMem.Used;
	--Arena->TempCount;
}

push_group *
AllocatePushGroup(memory_arena *Arena, int MaxPushBufferSize)
{
	push_group *Result = PushStruct(Arena, push_group);
	Result->PushBufferBase = (uint8 *)PushSize(Arena, MaxPushBufferSize);
	Result->MaxPushBufferSize = MaxPushBufferSize;
	Result->PushBufferSize = 0;

	return(Result);
}

#define PushBufferElement(Group, type) (type *)PushBufferElement_(Group, sizeof(type), PushBufferEntryType_##type)
inline push_buffer_header *
PushBufferElement_(push_group *Group, int Size, push_buffer_entry_type Type)
{
	push_buffer_header *Result = 0;

	if((Group->PushBufferSize + Size) < Group->MaxPushBufferSize)
	{
		Result = (push_buffer_header *)(Group->PushBufferBase + Group->PushBufferSize);
		Result->Type = Type;
		Group->PushBufferSize += Size;
	}
	else
	{
		InvalidCodePath;
	}

	return(Result);
}

void
PushFirstType(push_group *Group, int X, int Y)
{
	buffer_entry_first *Entry = PushBufferElement(Group, buffer_entry_first);
	if(Entry)
	{
		Entry->X = X;
		Entry->Y = Y;
	}
	else
	{
		InvalidCodePath;
	}
}

void
StrCopy(char *A, char *B)
{
	// TODO(rick): pass max length
	while(*A)
	{
		*B++ = *A++;
	}
}

void
PushSecondType(push_group *Group, char *Name, int Count)
{
	buffer_entry_second *Entry = PushBufferElement(Group, buffer_entry_second);
	if(Entry)
	{
		Entry->Count = Count;
		StrCopy(Name, Entry->Name);
	}
	else
	{
		InvalidCodePath;
	}
}

void
PushThirdType(push_group *Group, char *Class, char *Race, uint8 Level,
			  int Health, int Mana, int Experience)
{
	buffer_entry_third *Entry = PushBufferElement(Group, buffer_entry_third);
	if(Entry)
	{
		StrCopy(Class, Entry->Class);
		StrCopy(Race, Entry->Race);
		Entry->Level = Level;
		Entry->Health = Health;
		Entry->Mana = Mana;
		Entry->Experience = Experience;
	}
	else
	{
		InvalidCodePath;
	}
}

void
ProcessPushBuffer(push_group *Group)
{
	for(size_t BaseAddress = 0;
		BaseAddress < Group->PushBufferSize;
	   )
	{
		push_buffer_header *Header = (push_buffer_header *)(Group->PushBufferBase + BaseAddress);
		switch(Header->Type)
		{
			case PushBufferEntryType_buffer_entry_first:
			{
				buffer_entry_first *Entry = (buffer_entry_first *)Header;
				printf("Found entry type: First\n");
				printf("Position: (%d, %d)\n", Entry->X, Entry->Y);

				BaseAddress += sizeof(*Entry);
			} break;

			case PushBufferEntryType_buffer_entry_second:
			{
				buffer_entry_second *Entry = (buffer_entry_second*)Header;
				printf("Found entry type: Second\n");
				printf("(%d) %s\n", Entry->Count, Entry->Name);

				BaseAddress += sizeof(*Entry);
			} break;

			case PushBufferEntryType_buffer_entry_third:
			{
				buffer_entry_third *Entry = (buffer_entry_third *)Header;
				printf("(%d) %s %s\n", Entry->Level, Entry->Race, Entry->Class);
				printf("HP: %d, MP: %d, XP: %d\n", Entry->Health, Entry->Mana, Entry->Experience);
			
				BaseAddress += sizeof(*Entry);
			} break;

			InvalidDefaultCase;
		}
	}
}

int main(void)
{
	application_memory AppMemory;
	AppMemory.PermanentStorageSize = 0;
	AppMemory.TransientStorageSize = Megabyte(20);
	AppMemory.PermanentStorage = 0;
	AppMemory.TransientStorage = malloc(Megabyte(20));

	for(int LoopCount = 0;
		LoopCount < 10;
		++LoopCount)
	{
		transient_state *TranState = (transient_state *)AppMemory.TransientStorage;
		// NOTE(rick): Simulate transient memory being wiped out between
		// iterations
		if((LoopCount % 3 == 0) && LoopCount != 0)
		{
			if(TranState->TranArena.Base)
			{
				ZeroMemory(TranState->TranArena.Base, (size_t)Megabyte(4));
			}

			*TranState = {0};
		}

		if(!TranState->IsInitialized)
		{
			InitializeArena(&TranState->TranArena, Megabyte(5), (uint8 *)AppMemory.TransientStorage + sizeof(transient_state));
			TranState->IsInitialized = true;
		}

		temporary_memory PushBufferMemory = BeginTemporaryMemory(&TranState->TranArena);

		push_group *PushGroup = AllocatePushGroup(&TranState->TranArena, Megabyte(4));

		PushFirstType(PushGroup, 0, 0);
		PushFirstType(PushGroup, 1, 1);
		PushSecondType(PushGroup, "Product One", 5);
		PushSecondType(PushGroup, "Product Two", 1);
		PushThirdType(PushGroup, "Warrior", "Human", 110, 500, 0, 97);

		ProcessPushBuffer(PushGroup);
		printf("\n-----\n\n");

		EndTemporaryMemory(PushBufferMemory);
		Assert(TranState->TranArena.TempCount == 0);
	}

	free(AppMemory.TransientStorage);

	return 0;
}
